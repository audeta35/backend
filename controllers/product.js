// use strict
"use strict";
require('dotenv').config();

// module
const { v4: uuidv4 } = require('uuid');
const md5 = require('md5');

// file
const response = require("../responses/index");
const conn = require("../config/database");

exports.getAllProductById = (req, res) => {

    let {id} = req.body

    let query = `SELECT * FROM product WHERE id_services=?`;

    conn.query(query, [id], (err, data, field) => {
        if(!err) {
            response.success(res, data);
        } else {
            res.status(422).send(err);
        }
    })
}