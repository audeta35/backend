// use strict
"use strict";
require('dotenv').config();

// module
const { v4: uuidv4 } = require('uuid');
const md5 = require('md5');

// file
const response = require("../responses/index");
const conn = require("../config/database");

exports.addTransaction = (req, res) => {
    let {idUser, detailTransaction} = req.body;
    let uuid = uuidv4();
    let status = "pending"
    let query = `INSERT INTO transaction SET id=?, id_user=?, detail_transaction=?, status=?`;

    conn.query(query, [uuid, idUser, detailTransaction, status], (err, result, field) => {
        if(!err) {
            response.success(res, result)
        } else {
            return res.status(422).send(err)
        }
    })
}

exports.getTransactionById = (req, res) => {
    let { idUser } = req.body;
    let query = `SELECT * FROM transaction WHERE id_user=?`

    conn.query(query, [idUser], (err, data, field) => {
        if(!err) {
            response.success(res, data)
        } else {
            return res.status(422).send(err)
        }
    })
}

exports.getInvoiceUser = (req, res) => {
    let { id_transaction } = req.body;
    let query = `SELECT * FROM transaction WHERE id=?`;

    conn.query(query, [id_transaction], (err, data, field) => {
        if(!err) {
            response.success(res, data);
        } else {
            res.status(422).send(err)
        }
    })
}