// use strict
"use strict";
require('dotenv').config();

// module
const { v4: uuidv4 } = require('uuid');
const md5 = require('md5');
// file
const response = require("../responses/index");
const conn = require("../config/database");

exports.getAllUser = (req, res) => {
    let query = `SELECT * FROM users`

    conn.query(query, [], (err, data, field) => {
        if(!err) {
            response.success(res, data)
        } else {
            res.status(422).send(err)
        }
    })
}

exports.getUserByToken = (req, res) => {
    let {token} = req.body;
    let query = `SELECT * FROM users WHERE token=?`;

    conn.query(query, [token], (err, data, field) => {
        if(!err) {
            response.success(res, data)
        } else {
            res.status(422).send(err)
        }
    })
}

exports.getUserById = (req, res) => {
    let { id } = req.body;
    let query = `SELECT username, phone FROM users WHERE id=?`;

    conn.query(query, [id], (err, data, field) => {
        if (!err) {
            response.success(res, data)
        } else {
            res.status(422).send(err)
        }
    })
}

exports.addUser = (req, res) => {
    
    let id = uuidv4();
    let { username, email, phone, password, address, status } = req.body;

    let passwordEncrypt = md5(password);

    let query = `INSERT INTO users set id=?, username=?, email=?, phone=?, password=?, address=?, status=?`;

    conn.query(query, [id, username, email, phone, passwordEncrypt, address, status], (err, result, field) => {
        if(!err) {
            response.success(res, result);
        } else {
            return res.status(422).send(err)
        }
    })
}


exports.getAddressUser = (req, res) => {
    let { token } = req.body;
    let query = `SELECT address FROM users WHERE token=?`;
    
    conn.query(query, [token], (err, data, field) => {
        if(!err) {
            response.success(res, data);
        }
        else {
            return res.status(422).send(err)
        }
    })
}