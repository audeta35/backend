// use strict
"use strict";
require('dotenv').config();

// module
const { v4: uuidv4 } = require('uuid');
const md5 = require('md5');

// file
const response = require("../responses/index");
const conn = require("../config/database");

exports.loginUser = (req, res) => {
    let {email, password} = req.body;
    let query = `SELECT * FROM users WHERE email=? AND password=?`;
    let token = uuidv4();
    let passwordEncrypt = password ? md5(password) : "";

    conn.query(query, [email, passwordEncrypt], (err, data, field) => {
        if(!err) {
            if (data?.length === 1) {
                let queryUpdateToken = `UPDATE users SET token=? WHERE id=?`;
                conn.query(queryUpdateToken, [token, data[0].id], (err2, result, field) => {
                    if(!err2) {
                       conn.query(query, [email, passwordEncrypt], (err3, data2, field2) => {
                           if(!err3) {
                               response.loginSuccess(res, data2, token)
                           }
                           else {
                               res.status(422).send(err)
                           }
                       })
                    } else {
                        console.error("error while update token")
                        res.status(422).send(err)
                    }
                })
            } else {
                response.loginFailed(res)
            }
        } else {
            res.status(422).send(err)
        }
    })
}

exports.getToken = (req, res) => {
    let { token } = req.body;
    let query = `SELECT id, token FROM users WHERE token=?`;
    conn.query(query, [token], (err, data, field) => {
        if(!err) {
            response.success(res, data);
        } else {
            res.status(422).send(err)
        }
    })
}