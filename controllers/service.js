// use strict
"use strict";
require('dotenv').config();

// module
const { v4: uuidv4 } = require('uuid');
const md5 = require('md5');

// file
const response = require("../responses/index");
const conn = require("../config/database");

exports.getAllService = (req, res) => {
    let query = `SELECT * FROM services`;

    conn.query(query, [], (err, data, field) => {
        if (!err) {
            response.success(res, data);
        } else {
            res.status(422).send(err);
        }
    })
}