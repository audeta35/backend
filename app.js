const express = require('express');
const bodyParser = require('body-parser');
const port = process.env.PORT || 3100;
const server = express();
const app = require('http').Server(server);
const io = require('socket.io')(app);
const cors = require('cors');

// routes
const indexRoutes = require('./routes/index');
const userRoutes = require('./routes/users');
const authRoutes = require('./routes/auth');
const transactionRoutes = require('./routes/transaction');
const productRoutes = require('./routes/product');
const serviceRoutes = require('./routes/service');

server.use(function (req, res, next) {
    req.io = io;
    next();
});

server.use(bodyParser.urlencoded({ extended: true }));
server.use(bodyParser.json());

let whiteList = ["http://localhost:4200", "http://localhost:3000"];
server.use(
    // cors({
    //   origin: (origin, cb) => {
    //     if (whiteList.indexOf(origin) !== -1) {
    //       cb(null, true);
    //     } else {
    //       cb(new Error("Not allowed by CORS"));
    //     }
    //   },
    //   methods: ["GET", "POST", "PUT", "PATCH", "DELETE"],
    // })
    cors()
);

// Routes
server.use('/', indexRoutes);
server.use('/users', userRoutes);
server.use('/auth', authRoutes);
server.use('/transaction', transactionRoutes);
server.use('/product', productRoutes);
server.use('/service', serviceRoutes);

app.listen(port);
console.info(`Server listening on port ${port}`);