const express = require('express');
const routes = express.Router();
const authController = require('../controllers/auth');

routes.post('/login', authController.loginUser);
routes.post('/get-token', authController.getToken);

module.exports = routes;