const express = require('express');
const routes = express.Router();
const transactionController = require('../controllers/transaction');

routes.post('/set-invoice', transactionController.addTransaction);
routes.post('/get-by-id', transactionController.getTransactionById);
routes.post('/get-invoice', transactionController.getInvoiceUser);

module.exports = routes;