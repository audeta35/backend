const express = require('express');
const routes = express.Router();
const productController = require('../controllers/product');

routes.post('/get-product', productController.getAllProductById);

module.exports = routes;