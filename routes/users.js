"use strict";

const express = require("express");
const routes = express.Router();
const usersController = require("../controllers/users");

routes.post('/', usersController.getAllUser);
routes.post('/add-users', usersController.addUser);
routes.post('/get-by-token', usersController.getUserByToken);
routes.post('/get-address', usersController.getAddressUser);
routes.post('/get-profile', usersController.getUserById);

module.exports = routes;