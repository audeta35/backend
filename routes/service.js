const express = require('express');
const routes = express.Router();
const serviceController = require('../controllers/service');

routes.post('/get-all', serviceController.getAllService);

module.exports = routes;